import time
from tkinter import *

from quiz_brain import QuizBrain

THEME_COLOR = "#375362"


class QuizUI:
    def __init__(self, quiz_brain: QuizBrain):
        self.quiz = quiz_brain
        self.window = Tk()
        self.window.title('Quizz App')
        self.window.config(padx=20, pady=20, background=THEME_COLOR)

        self.score_label = Label(text="Score: 0", font=("Arial", 16, "bold"), background=THEME_COLOR, fg='white')
        self.score_label.grid(row=0, column=1)

        self.canvas = Canvas(width=300, height=250, background="#FFFFFF")
        self.question = self.canvas.create_text(150, 125, text="Kanye Quote Goes HERE", width=250,
                                                font=("Arial", 15, "italic bold"),
                                                fill=THEME_COLOR)
        self.get_next_question()
        self.canvas.grid(row=1, column=0, columnspan=2, pady=10)

        true_img = PhotoImage(file="images/true.png")
        false_img = PhotoImage(file="images/false.png")
        self.affirmative_btn = Button(image=true_img, highlightthickness=0, command=self.on_affirmative_btn_clicked)
        self.affirmative_btn.grid(column=1, row=2, pady=10)
        self.negative_btn = Button(image=false_img, highlightthickness=0, command=self.on_negative_btn_clicked)
        self.negative_btn.grid(column=0, row=2, pady=10)

        self.window.mainloop()

    def get_next_question(self):
        self.canvas.config(background="#FFFFFF")
        if self.quiz.still_has_questions():
            question_text = self.quiz.next_question()
            self.canvas.itemconfig(self.question, text=question_text)
        else:
            self.canvas.itemconfig(self.question, text="Game Over!")
            self.affirmative_btn.config(state="disabled")
            self.negative_btn.config(state="disabled")

    def on_affirmative_btn_clicked(self):
        is_answer_correct = self.quiz.check_answer("True")
        self.examine_answer(is_answer_correct)

    def on_negative_btn_clicked(self):
        is_answer_correct = self.quiz.check_answer("False")
        self.examine_answer(is_answer_correct)

    def examine_answer(self, is_answer_right: bool):
        # update score_label
        self.score_label.config(text=f"Score: {self.quiz.score}")
        # canvas turns GREEN on right answer || RED when wrong
        if is_answer_right:
            self.canvas.config(background="#00FF00")
        else:
            self.canvas.config(background="#FF0000")
        # after 1 sec, move to next question
        self.window.after(1000, self.get_next_question)
